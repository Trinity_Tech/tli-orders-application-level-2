package com.tli.orders.OrderManagement;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import static lombok.AccessLevel.PROTECTED;

@Entity
@Getter(PROTECTED)
@NoArgsConstructor(access = PROTECTED)
@Setter(PROTECTED)
@Table(name = "order_statuses")
class OrderStatus {

    @Id
    @Column(name = "id", insertable = true, updatable = false, nullable = false)
    private Integer id;

    @Column(name = "name")
    private String name;
}
