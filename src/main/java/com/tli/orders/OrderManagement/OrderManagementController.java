package com.tli.orders.OrderManagement;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RequestMapping("/api/v1")
@RequiredArgsConstructor
@RestController
class OrderManagementController {

    // TODO: Add the appropriate request mapping annotation
    // TODO: Validate the request

    public ResponseEntity<OrderDetails> create(CreateOrderRequest request) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    // Endpoint that can be used during manual testing
    // Note: this is not covered by tests as it is not required
    @GetMapping(value = "/orders/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<OrderDetails> findOne(@PathVariable Long id) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
        // return ResponseEntity.ok(orderManagement.findById(id).details());
    }

    // TODO: Optional: create endpoint to cancel an order
    // Note: this is not covered by tests as it is not required
}
