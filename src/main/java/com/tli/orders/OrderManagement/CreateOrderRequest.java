package com.tli.orders.OrderManagement;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;

@AllArgsConstructor
@Data
@NoArgsConstructor
public final class CreateOrderRequest {

    // TODO: Add validation
    // @NotNull(message = "Line items must not be null")
    @Valid
    private List<LineItemRequest> items;

    @AllArgsConstructor
    @Builder
    @NoArgsConstructor
    @Data
    public static final class LineItemRequest {

        // TODO: Add validation
        private String name;

        /*@NotNull(message = "Item price is required")
        @Digits(integer = 10, fraction = 2, message = "Item price must not have more than 10 digits or 2 decimal places")
        @DecimalMin(value = "0.00", message = "Item price must not be negative")*/
        private BigDecimal price;

        private BigDecimal quantity;
    }
}
