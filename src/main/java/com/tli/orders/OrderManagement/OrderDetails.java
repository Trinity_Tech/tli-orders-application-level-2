package com.tli.orders.OrderManagement;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Builder
@Data
public final class OrderDetails {

    private final Long id;
    private final String status;
    private final List<LineItemDetails> lineItems;

    @Builder
    @Data
    static class LineItemDetails {

        private final Integer number;
        private final String name;
        private final BigDecimal price;
        private final BigDecimal quantity;
    }
}
