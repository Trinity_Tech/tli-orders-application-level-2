package com.tli.orders.OrderManagement;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

import static java.lang.String.format;

@RequiredArgsConstructor
@Service
class OrderManagement {

    private final Orders orders;

    // TODO: Write a use case that creates a new order with requested line items
    // The order should be defaulted to New status
    // Each line item
        // Should have a unique number within the order
        // Price should not be negative
        // Quantity should be at least 1
        // A line item should not exist without an order.
            // Hint: don't create a separate repository for line items.
    // Make sure the use case runs in a single transaction
    public Order handle(CreateOrderRequest request) {
        return null;
    }

    private OrderStatus findNewStatus() {
        return orders.findStatusByName("New")
                .orElseThrow(() -> new EntityNotFoundException("Could not find order status: New"));
    }

    public Order findById(Long id) {
        return orders.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(format("Could not find order id: %d", id)));
    }
}
