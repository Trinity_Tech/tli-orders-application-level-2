package com.tli.orders.OrderManagement;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;

import static lombok.AccessLevel.PROTECTED;

@Entity
@Getter(PROTECTED)
@NoArgsConstructor(access = PROTECTED)
@Setter(PROTECTED)
@Table(name = "order_line_items")
class LineItem {

    @EmbeddedId
    private LineItemId id = new LineItemId();

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private BigDecimal price;

    @Column(name = "quantity")
    private BigDecimal quantity;

    // TODO: Create order association
    //private Order order;

    @Column(name = "created_date", insertable = true, updatable = false)
    private Instant createdDate = Instant.now();

    @Column(name = "created_by")
    private Long createdBy = 1L;

    @Column(name = "modified_date", insertable = true, updatable = true)
    private Instant modifiedDate = Instant.now();

    @Column(name = "modified_by")
    private Long modifiedBy = 1L;

    @Embeddable
    @Getter(PROTECTED)
    @Setter(PROTECTED)
    static class LineItemId implements Serializable {

        @Column(name = "number")
        private Integer number;

        @Column(name = "order_id")
        private Long orderId;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            LineItemId that = (LineItemId) o;
            return Objects.equals(number, that.number) && Objects.equals(orderId, that.orderId);
        }

        @Override
        public int hashCode() {
            return Objects.hash(number, orderId);
        }
    }
}
