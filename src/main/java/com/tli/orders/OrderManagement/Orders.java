package com.tli.orders.OrderManagement;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

interface Orders extends JpaRepository<Order, Long> {

    // This may be used during manual testing
    /*@Query("Select o From Order o Join Fetch o.lineItems li Join Fetch o.status s Where o.id = :id")
    Optional<Order> findById(Long id);*/

    @Query("Select os From OrderStatus os Where os.name = :name")
    Optional<OrderStatus> findStatusByName(String name);
}
