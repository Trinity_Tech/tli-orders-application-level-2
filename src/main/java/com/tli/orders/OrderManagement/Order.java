package com.tli.orders.OrderManagement;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static lombok.AccessLevel.PROTECTED;

@Entity
@Getter(PROTECTED)
@NoArgsConstructor(access = PROTECTED)
@Setter(PROTECTED)
@Table(name = "orders")
class Order {

    @Id
    @Column(name = "id", insertable = true, updatable = false)
    private Long id;

    // TODO: Add order status association
    //private OrderStatus status;

    // TODO: Add line item association
    // This should not be transient
    @Transient
    private List<LineItem> lineItems = new ArrayList<>();

    @Column(name = "created_date", insertable = true, updatable = false)
    private Instant createdDate = Instant.now();

    @Column(name = "created_by")
    private Long createdBy = 1L;

    @Column(name = "modified_date", insertable = true, updatable = true)
    private Instant modifiedDate = Instant.now();

    @Column(name = "modified_by")
    private Long modifiedBy = 1L;

    static Order createFrom(CreateOrderRequest request, OrderStatus orderStatus) {
        return null;
    }
}
