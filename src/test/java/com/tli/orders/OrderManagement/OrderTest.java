package com.tli.orders.OrderManagement;

import org.junit.jupiter.api.Test;

import java.util.List;

import static java.util.stream.Collectors.toSet;
import static org.assertj.core.api.Assertions.assertThat;

class OrderTest {

    @Test
    void createsANewOrderWithEachLineItemHavingADistinctNumber() {
        var request = new CreateOrderRequest(List.of(
                CreateOrderRequest.LineItemRequest.builder()
                        .name("Widget 1")
                        .build(),
                CreateOrderRequest.LineItemRequest.builder()
                        .name("Widget 2")
                        .build(),
                CreateOrderRequest.LineItemRequest.builder()
                        .name("Widget 3")
                        .build()
        ));

        var newOrder = Order.createFrom(request, new OrderStatus());

        var uniqueLineItemNumbers = newOrder.getLineItems().stream().map(li -> li.getId().getNumber()).collect(toSet());
        assertThat(uniqueLineItemNumbers.size()).isEqualTo(newOrder.getLineItems().size());
    }
}