package com.tli.orders.OrderManagement;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toSet;
import static org.assertj.core.api.Assertions.assertThat;

class CreateOrderRequestValidationTest {

    private Validator validator;

    @BeforeEach
    void setUp() {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    @Test
    void requiresNonNullListOfItems() {
        var request = new CreateOrderRequest();

        var violations = validator.validate(request);

        assertThat(messagesOf(violations)).contains("Line items must not be null");
    }

    @Test
    void requiresItemNameToNotBeBlank() {
        var request = new CreateOrderRequest(
                List.of(lineItemRequest().name("").build())
        );

        var violations = validator.validate(request);

        assertThat(messagesOf(violations)).contains("Item name must not be blank");
    }

    @Test
    void requiresItemPriceToNotBeNull() {
        var request = new CreateOrderRequest(List.of(
                lineItemRequest().price(null).build()
        ));

        var violations = validator.validate(request);

        assertThat(messagesOf(violations)).contains("Item price is required");
    }

    @Test
    void requiresItemPriceToBeNonNegative() {
        var request = new CreateOrderRequest(List.of(
                lineItemRequest().price(new BigDecimal("-12.34")).build()
        ));

        var violations = validator.validate(request);

        assertThat(messagesOf(violations)).contains("Item price must not be negative");
    }

    @Test
    void requiresItemPriceToBeNoMoreThan10DigitsAnd2DecimalPlaces() {
        var request = new CreateOrderRequest(List.of(
                lineItemRequest().price(new BigDecimal("12345678912.3456")).build()
        ));

        var violations = validator.validate(request);

        assertThat(messagesOf(violations)).contains("Item price must not have more than 10 digits or 2 decimal places");
    }

    @Test
    void requiresQuantityToNotBeNull() {
        var request = new CreateOrderRequest(List.of(
                lineItemRequest().quantity(null).build()
        ));

        var violations = validator.validate(request);

        assertThat(messagesOf(violations)).contains("Item quantity is required");
    }

    @Test
    void requiresItemQuantityToBeAtLeast1() {
        var request = new CreateOrderRequest(List.of(
                lineItemRequest().quantity(new BigDecimal("0.78")).build()
        ));

        var violations = validator.validate(request);

        assertThat(messagesOf(violations)).contains("Item quantity must be at least 1");
    }

    @Test
    void requiresItemQuantityToBeNoMoreThan10DigitsAnd2DecimalPlaces() {
        var request = new CreateOrderRequest(List.of(
                lineItemRequest().quantity(new BigDecimal("98765432198.7654")).build()
        ));

        var violations = validator.validate(request);

        assertThat(messagesOf(violations)).contains("Item quantity must not have more than 10 digits or 2 decimal places");
    }

    private CreateOrderRequest.LineItemRequest.LineItemRequestBuilder lineItemRequest() {
        return CreateOrderRequest.LineItemRequest.builder()
                .name("Widget 1")
                .price(new BigDecimal("0.00"))
                .quantity(new BigDecimal("1.00"));
    }

    private Set<String> messagesOf(Set<ConstraintViolation<CreateOrderRequest>> violations) {
        return violations.stream().map(ConstraintViolation::getMessage).collect(toSet());
    }
}
