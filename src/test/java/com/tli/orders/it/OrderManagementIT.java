package com.tli.orders.it;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tli.orders.OrderManagement.CreateOrderRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.util.List;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = RANDOM_PORT)
class OrderManagementIT {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mvc;

    @BeforeEach
    void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    void createsANewOrder() throws Exception {
        mvc.perform(post("/api/v1/orders")
                .contentType(APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(new CreateOrderRequest(List.of(
                        CreateOrderRequest.LineItemRequest.builder()
                                .name("Widget 1")
                                .price(new BigDecimal("12.34"))
                                .quantity(new BigDecimal("15"))
                                .build()
                )))))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.status").value("New"))
                .andExpect(jsonPath("$.lineItems").isArray())
                .andExpect(jsonPath("$.lineItems[0].number").exists())
                .andExpect(jsonPath("$.lineItems[0].name").value("Widget 1"))
                .andExpect(jsonPath("$.lineItems[0].price").value(12.34))
                .andExpect(jsonPath("$.lineItems[0].quantity").value(15));
    }

    @Test
    void createsANewOrderWithMultipleLineItems() throws Exception {
        mvc.perform(post("/api/v1/orders")
                        .contentType(APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(new CreateOrderRequest(List.of(
                                CreateOrderRequest.LineItemRequest.builder()
                                        .name("Widget 1")
                                        .price(new BigDecimal("12.34"))
                                        .quantity(new BigDecimal("15"))
                                        .build(),
                                CreateOrderRequest.LineItemRequest.builder()
                                        .name("Widget 2")
                                        .price(new BigDecimal("56.78"))
                                        .quantity(new BigDecimal("114"))
                                        .build()
                        )))))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.status").value("New"))
                .andExpect(jsonPath("$.lineItems").isArray())
                .andExpect(jsonPath("$.lineItems[0].number").exists())
                .andExpect(jsonPath("$.lineItems[0].name").value("Widget 1"))
                .andExpect(jsonPath("$.lineItems[0].price").value(12.34))
                .andExpect(jsonPath("$.lineItems[0].quantity").value(15))
                .andExpect(jsonPath("$.lineItems[1].number").exists())
                .andExpect(jsonPath("$.lineItems[1].name").value("Widget 2"))
                .andExpect(jsonPath("$.lineItems[1].price").value(56.78))
                .andExpect(jsonPath("$.lineItems[1].quantity").value(114));
    }

    @Test
    void returnsBadRequestWhenCreateOrderRequestIsInvalid() throws Exception {
        mvc.perform(post("/api/v1/orders")
                        .contentType(APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(new CreateOrderRequest())))
                .andExpect(status().isBadRequest());
    }
}
